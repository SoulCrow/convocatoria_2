/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convo2;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Jean Carlos
 */
public class Notas {
    ArrayList<Estudiante> notas  = new ArrayList<Estudiante>();;
    
    
    public void Menu(){
        int op = 0;
        Scanner s = new Scanner(System.in);
        
        do{
        System.out.println("1. Registrar Estudiante");
        System.out.println("2. Editar Estudiante");
        System.out.println("3. Buscar Estudiante");
        System.out.println("4. Ver todos los Estudiante");
        System.out.println("5. Eliminar Estudiante");
        System.out.println("6. Salir");
        System.out.println("Seleccione una opcion");
        op = s.nextInt();
        
        switch(op){
            case 1:
                Registro();
                break;
            case 2:
                Editar();
                break;
            case 3:
                buscarEstudiante();
                break;
            case 4:
                verEstudiante();
                break;
            case 5:
                Eliminar();
                break;
            case 6:
                System.exit(0);
                break;
            default:
                System.out.println("Seleccione una opcion correcta....");
                break;
        }
        }while(true);


    }
    
    public void Registro(){
        
        Scanner s = new Scanner(System.in);
        Scanner s1 = new Scanner(System.in);
        Scanner s2 = new Scanner(System.in);
        double nt[];
        Estudiante es = new Estudiante();
        
        System.out.println("Ingrese el primer nombre del estudiante: ");
        es.setPrimer_nombre(s.nextLine());
        System.out.println("Ingrese el segundo nombre del estudiante: ");
        es.setSegundo_nombre(s.nextLine());
        System.out.println("Ingrese el primer apellido del estudiante: ");
        es.setPrimer_apellido(s.nextLine());
        System.out.println("Ingrese el primer apellido del estudiante: ");
        es.setSegundo_apellido(s.nextLine());
        System.out.println("Ingrese el carnet del estudiante: ");
        es.setCarnet(s.nextLine());
        System.out.println("Ingrese la cantidad de asignaturas: ");
        int cant = s1.nextInt();
        nt = new double[cant];
        for(int i = 0; i < cant; i++){
            System.out.println("Ingrese la nota "+(i+1)+": ");
            nt[i] = s2.nextDouble();
        }
        
        es.setAsignatura(nt);
        notas.add(es);        
    }
    
    public void Editar(){
        String carnet = "";

        Scanner s = new Scanner(System.in);
        Scanner s1 = new Scanner(System.in);
        Scanner s2 = new Scanner(System.in);
        double nt[] = null;
        System.out.println("Ingrese el carnet del estudiante que desea editar: ");
        carnet = s.nextLine();
        for(Estudiante e : notas){
            if(e.getCarnet().equals(carnet)){
                e.setPrimer_nombre(s.nextLine());
                System.out.println("Ingrese el primer nombre del estudiante: ");
                e.setPrimer_nombre(s.nextLine());
                System.out.println("Ingrese el segundo nombre del estudiante: ");
                e.setSegundo_nombre(s.nextLine());
                System.out.println("Ingrese el primer apellido del estudiante: ");
                e.setPrimer_apellido(s.nextLine());
                System.out.println("Ingrese el primer apellido del estudiante: ");
                e.setSegundo_apellido(s.nextLine());
                System.out.println("Ingrese el carnet del estudiante: ");
                e.setCarnet(s.nextLine());
                System.out.println("Ingrese la cantidad de asignaturas: ");
                int cant = s1.nextInt();
                nt = new double[cant];
                for(int i = 0; i < cant; i++){
                    System.out.println("Ingrese la nota "+(i+1)+": ");
                    nt[i] = s2.nextDouble();
                    
                }
                e.setAsignatura(nt);
            }
        }
        
        
        
        
    }
    
    public void verEstudiante(){
        
        for(Estudiante es : notas){
            double promedio = 0;
            System.out.println("Carnet: "+es.getCarnet());
                System.out.print("Nombre del estudiante: "+es.getPrimer_nombre());
                System.out.print(" "+es.getSegundo_nombre());
                System.out.print(" "+es.getPrimer_apellido());
                System.out.print(" "+es.getSegundo_apellido());
                for(int i = 0; i < es.getAsignatura().length; i++){
                    promedio += es.getAsignatura()[i];
                }
                promedio /= es.getAsignatura().length;
                System.out.println("Promedio: "+promedio);
            
        }
    }
    
    public void buscarEstudiante(){

        String carnet = "";
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese el carnet del estudiante que desea buscar: ");
        carnet = s.nextLine();
        for(Estudiante es : notas){
            if(es.getCarnet().equals(carnet)){
            double promedio = 0;
            System.out.println("Carnet: "+es.getCarnet());
                System.out.print("Nombre del estudiante: "+es.getPrimer_nombre());
                System.out.print(" "+es.getSegundo_nombre());
                System.out.print(" "+es.getPrimer_apellido());
                System.out.print(" "+es.getSegundo_apellido());
                for(int i = 0; i < es.getAsignatura().length; i++){
                    promedio += es.getAsignatura()[i];
                }
                promedio /= es.getAsignatura().length;
                System.out.println("Promedio: "+promedio);
            
        }
      }
    }
    
    public void Eliminar(){
        String carnet = "";
        Estudiante es = new Estudiante();
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese el carnet del estudiante que desea eliminar: ");
        carnet = s.nextLine();
        int ct = 0;
        
        for(Estudiante e : notas){
            if(e.getCarnet().equals(carnet)){
                break;
            }
            ct++;
        }

        notas.remove(ct);
            
        
      }
    }


